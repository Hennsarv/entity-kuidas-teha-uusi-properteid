﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp23
{
    class Program
    {
        static Northwind22Entities ne = new Northwind22Entities();


        static void Main(string[] args)
        {
            foreach(var x in ne.Orders)
                Console.WriteLine($"{x.Customer.CompanyName} tellis {x.OrderTotal} raha eest seal hulga kalu {x.SeaFoodTotal}" );

            foreach(var x in ne.OrderDetails)
                Console.WriteLine(x.SalesAmount);

        }



    }

    partial class OrderDetail
    {
        public decimal SalesAmount
        {
            get => this.Quantity * this.UnitPrice;
        }

    }

    partial class Order
    {
        public decimal OrderTotal
        {
            get => this
                .Order_Details
                .Sum(x => x.SalesAmount);
        }

        public decimal SeaFoodTotal
        {
            get => this
                .Order_Details
                .Where(x => x.Product.CategoryID == 8)
                .Sum(x => x.SalesAmount);
        }
    }
}
